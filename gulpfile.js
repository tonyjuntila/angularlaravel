var elixir = require('laravel-elixir');
var config = elixir.config;

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss','index.scss','base.scss');
});

elixir(function(mix) {
    mix.copy('resources/assets/vendor/angular.js', 'public/vendor/angular.js');
    mix.copy('resources/assets/vendor/angular-route.js', 'public/vendor/angular-route.js');
    mix.copy('resources/assets/vendor/angular-resource.js', 'public/vendor/angular-resource.js');
});

elixir(function(mix) {
    mix.coffee(['**/*.coffee']);
});

elixir(function(mix) {
    mix.version(['vendor', 'css', 'js']);
});