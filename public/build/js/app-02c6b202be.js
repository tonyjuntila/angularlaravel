
/*global angular */


/**
 * The main TodoMVC app module
 *
 * @type {angular.Module}
 */

(function() {
  angular.module('todomvc', ['ngRoute', 'ngResource']).config(function($routeProvider) {
    'use strict';
    var routeConfig;
    routeConfig = {
      controller: 'TodoCtrl',
      templateUrl: 'resources/views/todo',
      resolve: {
        store: function(todoStorage) {
          return todoStorage.then(function(module) {
            module.get();
            return module;
          });
        }
      }
    };
    $routeProvider.when('/', routeConfig).when('/:status', routeConfig).otherwise({
      redirectTo: '/'
    });
  });

}).call(this);

//# sourceMappingURL=app.js.map
