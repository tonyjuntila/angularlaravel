###
# Welcome to the new js2coffee 2.0, now
# rewritten to use the esprima parser.
# try it out!
###

### global _ ###

do ->

	###jshint ignore:end ###

	redirect = ->
		if location.hostname == 'tastejs.github.io'
			location.href = location.href.replace('tastejs.github.io/todomvc', 'todomvc.com')
		return

	findRoot = ->
		base = location.href.indexOf('examples/')
		location.href.substr 0, base

	getFile = (file, callback) ->
		if !location.host
			return console.info('Miss the info bar? Run TodoMVC from a server to avoid a cross-origin error.')
		xhr = new XMLHttpRequest
		xhr.open 'GET', findRoot() + file, true
		xhr.send()

		xhr.onload = ->
			if xhr.status == 200 and callback
				callback xhr.responseText
			return

		return

	Learn = (learnJSON, config) ->
		if !(this instanceof Learn)
			return new Learn(learnJSON, config)
		template = undefined
		framework = undefined
		if typeof learnJSON != 'object'
			try
				learnJSON = JSON.parse(learnJSON)
			catch e
				return
		if config
			template = config.template
			framework = config.framework
		if !template and learnJSON.templates
			template = learnJSON.templates.todomvc
		if !framework and document.querySelector('[data-framework]')
			framework = document.querySelector('[data-framework]').dataset.framework
		@template = template
		if learnJSON.backend
			@frameworkJSON = learnJSON.backend
			@frameworkJSON.issueLabel = framework
			@append backend: true
		else if learnJSON[framework]
			@frameworkJSON = learnJSON[framework]
			@frameworkJSON.issueLabel = framework
			@append()
		@fetchIssueCount()
		return

	'use strict'

	###jshint ignore:start ###

	# Underscore's Template Module
	# Courtesy of underscorejs.org
	_ = ((_) ->

		_.defaults = (object) ->
			if !object
				return object
			argsIndex = 1
			argsLength = arguments.length
			while argsIndex < argsLength
				iterable = arguments[argsIndex]
				if iterable
					for key of iterable
						if object[key] == null
							object[key] = iterable[key]
				argsIndex++
			object

		# By default, Underscore uses ERB-style template delimiters, change the
		# following template settings to use alternative delimiters.
		_.templateSettings =
			evaluate: /<%([\s\S]+?)%>/g
			interpolate: /<%=([\s\S]+?)%>/g
			escape: /<%-([\s\S]+?)%>/g
		# When customizing `templateSettings`, if you don't want to define an
		# interpolation, evaluation or escaping regex, we need one that is
		# guaranteed not to match.
		noMatch = /(.)^/
		# Certain characters need to be escaped so that they can be put into a
		# string literal.
		escapes =
			'\'': '\''
			'\\': '\\'
			'\ud': 'r'
			'\n': 'n'
			'\u9': 't'
			'\u2028': 'u2028'
			'\u2029': 'u2029'
		escaper = /\\|'|\r|\n|\t|\u2028|\u2029/g
		# JavaScript micro-templating, similar to John Resig's implementation.
		# Underscore templating handles arbitrary delimiters, preserves whitespace,
		# and correctly escapes quotes within interpolated code.

		_.template = (text, data, settings) ->
			render = undefined
			settings = _.defaults({}, settings, _.templateSettings)
			# Combine delimiters into one regular expression via alternation.
			matcher = new RegExp([
					(settings.escape or noMatch).source
					(settings.interpolate or noMatch).source
					(settings.evaluate or noMatch).source
				].join('|') + '|$', 'g')
			# Compile the template source, escaping string literals appropriately.
			index = 0
			source = '__p+=\''
			text.replace matcher, (match, escape, interpolate, evaluate, offset) ->
				source += text.slice(index, offset).replace(escaper, (match) ->
					'\\' + escapes[match]
				)
				if escape
					source += '\'+\n((__t=(' + escape + '))==null?\'\':_.escape(__t))+\n\''
				if interpolate
					source += '\'+\n((__t=(' + interpolate + '))==null?\'\':__t)+\n\''
				if evaluate
					source += '\';\n' + evaluate + '\n__p+=\''
				index = offset + match.length
				match
			source += '\';\n'
			# If a variable is not specified, place data values in local scope.
			if !settings.variable
				source = 'with(obj||{}){\n' + source + '}\n'
			source = 'var __t,__p=\'\',__j=Array.prototype.join,' + 'print=function(){__p+=__j.call(arguments,\'\');};\n' + source + 'return __p;\n'
			try
				render = new Function(settings.variable or 'obj', '_', source)
			catch e
				e.source = source
				throw e
			if data
				return render(data, _)

			template = (data) ->
				render.call this, data, _

			# Provide the compiled function source as a convenience for precompilation.
			template.source = 'function(' + (settings.variable or 'obj') + '){\n' + source + '}'
			template

		_
	)({})
	if location.hostname == 'todomvc.com'
		((i, s, o, g, r, a, m) ->
			i['GoogleAnalyticsObject'] = r
			i[r] = i[r] or ->
					(i[r].q = i[r].q or []).push arguments
					return

			i[r].l = 1 * new Date
			a = s.createElement(o)
			m = s.getElementsByTagName(o)[0]
			a.async = 1
			a.src = g
			m.parentNode.insertBefore a, m
			return
		) window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga'
		ga 'create', 'UA-31081062-1', 'auto'
		ga 'send', 'pageview'

	Learn::append = (opts) ->
		aside = document.createElement('aside')
		aside.innerHTML = _.template(@template, @frameworkJSON)
		aside.className = 'learn'
		if opts and opts.backend
# Remove demo link
			sourceLinks = aside.querySelector('.source-links')
			heading = sourceLinks.firstElementChild
			sourceLink = sourceLinks.lastElementChild
			# Correct link path
			href = sourceLink.getAttribute('href')
			sourceLink.setAttribute 'href', href.substr(href.lastIndexOf('http'))
			sourceLinks.innerHTML = heading.outerHTML + sourceLink.outerHTML
		else
# Localize demo links
			demoLinks = aside.querySelectorAll('.demo-link')
			Array::forEach.call demoLinks, (demoLink) ->
				if demoLink.getAttribute('href').substr(0, 4) != 'http'
					demoLink.setAttribute 'href', findRoot() + demoLink.getAttribute('href')
				return
		document.body.className = (document.body.className + ' learn-bar').trim()
		document.body.insertAdjacentHTML 'afterBegin', aside.outerHTML
		return

	Learn::fetchIssueCount = ->
		issueLink = document.getElementById('issue-count-link')
		if issueLink
			url = issueLink.href.replace('https://github.com', 'https://api.github.com/repos')
			xhr = new XMLHttpRequest
			xhr.open 'GET', url, true

			xhr.onload = (e) ->
				parsedResponse = JSON.parse(e.target.responseText)
				if parsedResponse instanceof Array
					count = parsedResponse.length
					if count != 0
						issueLink.innerHTML = 'This app has ' + count + ' open issues'
						document.getElementById('issue-count').style.display = 'inline'
				return

			xhr.send()
		return

	redirect()
	getFile 'learn.json', Learn
	return

# ---
# generated by js2coffee 2.1.0