<!DOCTYPE html>
<html lang="en" @yield('ng-app')>
<head>
	@yield('head')
</head>
<body ng-app="todomvc">
	@yield('content')
	@yield('footer')
</body>
</html>
